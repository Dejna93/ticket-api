import axios from 'axios'

const authentication = {

    isAuthenticated(){
        const token= localStorage.getItem('token');
        if (token){
            return axios.get("http://localhost:8000/api/auth/token"
            ,{headers: {"Authorization": token}})
        }else{
            return new Promise(function (resolve, reject) {
                reject();
            })
        }
    },

    login(email, password, cb){
        const promise = axios.post("http://localhost:8000/api/users/login",
            {
                email: email,
                password: password,
            }
        );
        this.handleAuth(promise,cb);
    },
    logout(){
        const token = localStorage.getItem('token');
        localStorage.removeItem('token');
        axios.delete("http://localhost:8000/api/auth/token",
            {headers: {"Authorization": token}}
        );
        return true;
    },
    handleAuth(promise, cb) {
        promise.then((resp) => {
            if (resp.data.token){
                localStorage.setItem('token', resp.data.token);
                cb(true);
            }
        }).catch((error) => cb(false));
    }

}
export default authentication