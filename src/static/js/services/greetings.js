
import axios from 'axios'

const greeting = {

  fetch () {
    const token = localStorage.getItem('token');
    if(token) {
      return axios.get("http://localhost:8000/api/posts", {headers: {"Authorization": token}});
    } else {
      return new Promise(function(resolve, reject){ reject(); });
    }
  }

}

export default greeting