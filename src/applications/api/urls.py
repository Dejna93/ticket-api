from django.conf.urls import url
from django.contrib import admin

from .views import (
    ApplicationCreateAPIView,
    ApplicationDeleteAPIView,
    ApplicationDetailAPIView,
    ApplicationUpdateAPIView,
    ApplicationListAPIView,
    )

urlpatterns = [
    url(r'^$', ApplicationListAPIView.as_view(), name='list'),
    url(r'^create/$', ApplicationCreateAPIView.as_view(), name='create'),
    url(r'^(?P<slug>[\w-]+)/$', ApplicationDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<slug>[\w-]+)/edit/$', ApplicationUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<slug>[\w-]+)/delete/$', ApplicationDeleteAPIView.as_view(), name='delete'),
]
