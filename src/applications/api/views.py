from posts.api.permissions import IsOwnerOrReadOnly
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
 )
from posts.api.pagination import (
    PostLimitOffsetPagination,
    PostPageNumberPagination,
)
from .serializer import (
    ApplicationCreateUpdateSerializers,
    ApplicationDetailSerializer,
    ApplicationListSerializer,
)

from applications.models import Application

from django.db.models import Q
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.mixins import (
    DestroyModelMixin,
    UpdateModelMixin,
)
from posts.api.permissions import IsOwnerOrReadOnly



class ApplicationDetailAPIView(RetrieveAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationDetailSerializer
    lookup_field = 'slug'

class ApplicationUpdateAPIView(UpdateAPIView):
    queryset = Application.objects.all()
    serializer_class =  ApplicationCreateUpdateSerializers
    permission_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'slug'
    def partial_update(self, serializer):
        serializer.save(user= self.request.user)

class ApplicationDeleteAPIView(DestroyAPIView):
    queryset = Application.objects.all()
    serializer_class = ApplicationDetailSerializer
    lookup_field = 'slug'


class ApplicationCreateAPIView(CreateAPIView):

    serializer_class = ApplicationCreateUpdateSerializers
    permission_classes = [IsAuthenticated, IsAdminUser]
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)



class ApplicationListAPIView(ListAPIView):
    serializer_class = ApplicationListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title', 'version']
    pagination_class = PostPageNumberPagination #LimitOffsetPagination# PageNumberPagination


    def get_queryset(self, *args, **kwargs):
        queryset_list = Application.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(title__icontains=query) |
                Q(version__icontains=query)
            ).distinct()
        return queryset_list










