from rest_framework.serializers import(
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    ValidationError,
)

from applications.models import Application

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model

from accounts.api.serializers import UserDetailSerializer



class ApplicationDetailSerializer(ModelSerializer):
    class Meta:
        model = Application
        fields = [
            'id',
            'title',
            'version',
            'timestamp',

        ]
        read_only_fields = [
         #    'content_type',
         #   'object_id',
        ]

class ApplicationListSerializer(ModelSerializer):

    class Meta:
        model = Application
        fields = [
            'id',
            'title',
            'version',
            'timestamp',

        ]

class ApplicationCreateUpdateSerializers(ModelSerializer):
    class Meta:
        model = Application
        fields = [
            'id',
            'title',
            'version',
            'timestamp',
        ]

