from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
 )

from ticket.models import Ticket

from ticket.api.serializers import (
    TicketListSerializer,
    TicketCreateUpdateSerializers,
    TicketDetailSerializers,
)

from rest_framework.permissions import (
    AllowAny,
)
from rest_framework.response import Response
from rest_framework import status

from django.contrib.auth.models import User
from rest_framework.views import APIView
from django.db.models import Q
from ticket.models import State, Type
from django.http import Http404
from django.contrib.auth import get_user_model
User = get_user_model()

class TicketDetailAPIVIew(RetrieveAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketDetailSerializers

class TicketUpdateAPIVIEW(UpdateAPIView):
    """docstring for TicketUpdateAPIVIEW."""
    queryset = Ticket.objects.all()
    serializer_class = TicketCreateUpdateSerializers

    def perform_update(self,serializer):
        #userobj = User.objects.get(username=self.request.data["user"]["username"])
        state = State.objects.get(name = self.request.data["state"])
        type = Type.objects.get(name=self.request.data["type"])
        #serializer.save(user=userobj, state = state , type=type)
        serializer.save(state = state , type=type)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class TicketDeleteAPIView(DestroyAPIView):
    queryset = Ticket.objects.all()
    serializer_class = TicketDetailSerializers


class TicketCreateAPIView(CreateAPIView):
    serializer_class = TicketCreateUpdateSerializers
    queryset = Ticket.objects.all()
    def perform_create(self, serializer):
        userobj = User.objects.get(username=self.request.data["user"]["username"])
        state = State.objects.get(pk = self.request.data["state"])
        type = Type.objects.get(pk=self.request.data["type"])
        serializer.save(user=userobj,state = state , type=type)

        return Response(serializer.data, status=status.HTTP_201_CREATED)
"""    def create(self, request, *args, **kwargs):
        request.data["user"] = User.objects().get(username=request.data["user"])
        serializer = self.get_serializer(data = request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        header = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
"""
class TicketListAPIView(ListAPIView):
    serializer_class = TicketListSerializer

    def get_queryset(self,*args,**kwargs):
        queryset_list = Ticket.objects.all()
        username = self.request.GET.get("user", None)
        state = int(self.request.GET.get("state", 0))
        archive = self.request.GET.get("archive", None)
        if state:

            try:
                stateobj = State.objects.get(pk=state)
            #userobj = User.objects.get(username=username)
            except State.DoesNotExist:
                raise Http404
            queryset_list = queryset_list.exclude(state = stateobj.id)
            return queryset_list
        if username:
            try:
                userobj = User.objects.get(username=username)
            #userobj = User.objects.get(username=username)
            except User.DoesNotExist:
                raise Http404

            queryset_list = queryset_list.filter(
                user = userobj
            )
            return queryset_list
        return queryset_list
    """    if username and state:

            try:
                userobj = User.objects.get(username=username)
            #userobj = User.objects.get(username=username)
            except User.DoesNotExist:
                raise Http404
            try:
                stateobj = State.objects.get(pk=state)
            #userobj = User.objects.get(username=username)
            except State.DoesNotExist:
                raise Http404
            queryset_list = queryset_list.filter(
                user = userobj).exclude(state = stateobj.id)"""
