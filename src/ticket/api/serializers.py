from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    StringRelatedField,
    ChoiceField,
    RelatedField,
)
###TICKET
from ticket.models import Ticket, Type, State

###USER
from accounts.api.serializers import UserDetailSerializer

class StateSerializer(ModelSerializer):
    class Meta:
        model = State
        fields = [
        #    'id',
            'name',
        ]

class TypeSerializer(ModelSerializer):
    class Meta:
        model = Type
        fields = [
        #    'id',
            'name',
        ]




class TicketCreateUpdateSerializers(ModelSerializer):
    user = UserDetailSerializer(read_only=True)
    state = StringRelatedField()
    type = StringRelatedField()
    class Meta:
        model= Ticket
        fields = [
            'id',
            'title',
            'content',
            'state',
            'type',
            'user',
        ]



class TicketDetailSerializers(ModelSerializer):
##  dodanie pozniej messageS
    state = StringRelatedField()
    type = StringRelatedField()
    class Meta:
        model= Ticket
        fields = [
        'id',
        'title',
        'content',
        'state',
        'type',
        'create',
        'timestamp',
        ]


class TicketListSerializer(ModelSerializer):
    state = StringRelatedField()
    type = StringRelatedField()
    user = UserDetailSerializer(read_only=True)
    class Meta:
        model= Ticket
        fields = [
            'id',
            'title',
            'content',
            'type',
            'state',
            'create',
            'timestamp',
            'user',
        ]
