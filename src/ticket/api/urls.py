from django.conf.urls import url
from django.contrib import admin

from .views import (
    TicketDetailAPIVIew,
    TicketUpdateAPIVIEW,
    TicketDeleteAPIView,
    TicketCreateAPIView,
    TicketListAPIView
	)

urlpatterns = [
	url(r'^$', TicketListAPIView.as_view() , name='list'),
    url(r'^create/$', TicketCreateAPIView.as_view() , name='create'),
    url(r'^(?P<pk>[\w-]+)/$', TicketDetailAPIVIew.as_view(), name='detail'),
    url(r'^(?P<pk>[\w-]+)/edit/$',     TicketUpdateAPIVIEW.as_view(), name='update'),
    url(r'^(?P<pk>[\w-]+)/delete/$', TicketDeleteAPIView.as_view() , name='delete'),
]
