from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
# Create your models here.
TYPE_TICKET_CHOICES = (
    (1, 'Serwisowe'),
    (2, 'Projekt wewnętrzny'),
    (3, 'Projekt zewnętrzny'),
)
STATE_TICKET_CHOICES = (
    (1, 'Nowy'),
    (2, 'W przygotowaniu'),
    (3, 'Opóźnione'),
    (4, 'Testy'),
    (5, 'Zamknięty'),
)
class Type(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class State(models.Model):
    name = models.CharField(max_length=100)
    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name


class Ticket(models.Model):
    title = models.CharField(max_length=255)
    content = models.TextField()
    type = models.ForeignKey(Type, blank=False,null=False, related_name='type_ticket')
    state = models.ForeignKey(State, blank=False,null=False , related_name='state_ticket')
    #type = models.CharField(choices=TYPE_TICKET_CHOICES, max_length=30, default=0)
    #state = models.CharField(choices=STATE_TICKET_CHOICES, max_length=30, default=0)
    create = models.DateField(default=datetime.now, blank=True)
    timestamp = models.DateTimeField(default=datetime.now, blank=True)
    user = models.ForeignKey(User, default=1)
    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.title
