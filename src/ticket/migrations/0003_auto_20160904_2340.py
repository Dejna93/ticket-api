# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-09-04 23:40
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ticket', '0002_auto_20160904_2254'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='create',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
        migrations.AddField(
            model_name='ticket',
            name='timestamp',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
    ]
