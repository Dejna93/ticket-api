from django.contrib import admin

# Register your models here.
from .models import (Ticket,Type,State)

class TicketModelAdmin(admin.ModelAdmin):
	list_display = ["title","content","state","type"]
	list_display_links = ["content"]
	list_editable = ["title"]

	search_fields = ["title", "content"]
	class Meta:
		model = Ticket

class TypeModelAdmin(admin.ModelAdmin):
	list_display = ["id","name"]
	list_display_links = ["id"]
	list_editable = ["name"]

	search_fields = ["name"]
	class Meta:
		model = Type
class StateModelAdmin(admin.ModelAdmin):
	list_display = ["id","name"]
	list_display_links = ["id"]
	list_editable = ["name"]

	search_fields = ["name"]
	class Meta:
		model = State
admin.site.register(Ticket, TicketModelAdmin)
admin.site.register(Type, TypeModelAdmin)
admin.site.register(State, StateModelAdmin)
