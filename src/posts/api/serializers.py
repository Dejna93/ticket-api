from rest_framework.serializers import (
    ModelSerializer,
    HyperlinkedIdentityField,
    SerializerMethodField,
    ChoiceField,
)
from posts.models import Post
from posts.models import STATE_TICKET_CHOICES,TYPE_TICKET_CHOICES
from comments.api.serializer import (
CommentSerializer,
)
from comments.models import Comment
from accounts.api.serializers import UserDetailSerializer

from posts.models import STATE_TICKET_CHOICES,TYPE_TICKET_CHOICES
class PostCreateUpdateSerializers(ModelSerializer):
    type = ChoiceField(choices=TYPE_TICKET_CHOICES, default=1 )
    state = ChoiceField(choices=STATE_TICKET_CHOICES, default=1)
    class Meta:
        model = Post
        fields = [
        #    'id',
            'title',
            'state',
            'type',
            'slug',
            'content',
            'publish',
        ]

class PostDetailSerializers(ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='posts-api:detail',
        lookup_field='slug'
    )
    user = UserDetailSerializer(read_only=True)
    image = SerializerMethodField()
    markdown = SerializerMethodField()
    comments = SerializerMethodField()
    class Meta:
        model = Post
        fields = [
            'url',
            'id',
            'title',
            'user',
            'state',
            'type',
            'slug',
            'content',
            'publish',
            'image',
            'markdown',
            'comments',
        ]
    def get_image(self, obj):
        try:
            image = obj.image.url
        except:
            return None
        return image

    def get_markdown(self, obj):
        return obj.get_markdown()

    def get_user(self, obj):
        return str(obj.user.username)

    def get_comments(self,obj):
        content_type = obj.get_content_type
        object_id = obj.id
        c_qs = Comment.objects.filter_by_instance(obj)
        comments = CommentSerializer(c_qs, many=True).data
        return comments

class PostListSerializers(ModelSerializer):
    url = HyperlinkedIdentityField(
        view_name='posts-api:detail',
        lookup_field='slug'
    )
    state = SerializerMethodField()
    type = SerializerMethodField()
  #  user = UserDetailSerializer(read_only=True)
 #   delete_url = HyperlinkedIdentityField(
 #       view_name='posts-api:delete',
 #       lookup_field='slug'
 #   )
    class Meta:
        model = Post
        fields = [
            'url',
            'id',
            'title',
            'slug',
            'state',
            'type',
            'content',
            'publish',
   #        'user',
   #         'delete_url',
        ]


   # def get_user(self, obj):
    #    return str(obj.user.username)
    def get_state(self,obj):
        return STATE_TICKET_CHOICES[int(obj.state)][1]
    def get_type(self, obj):
        return TYPE_TICKET_CHOICES[int(obj.type)][1]
