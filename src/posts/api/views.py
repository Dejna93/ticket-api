from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
 )

from posts.models import Post
from posts.api.serializers import (
    PostListSerializers,
    PostDetailSerializers,
    PostCreateUpdateSerializers,

)
from django.db.models import Q
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)

from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)

from .permissions import IsOwnerOrReadOnly

from rest_framework.pagination import (
    LimitOffsetPagination,
    PageNumberPagination,
)

from .pagination import (
    PostLimitOffsetPagination,
    PostPageNumberPagination,
)

class PostDetailAPIView(RetrieveAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializers
    lookup_field = 'slug'

class PostUpdateAPIView(UpdateAPIView):
    queryset = Post.objects.all()
    serializer_class =  PostCreateUpdateSerializers
    #permission_classes = [IsAuthenticatedOrReadOnly]
    lookup_field = 'slug'
    #def partial_update(self, serializer):
    #    serializer.save(user= self.request.user)


class PostDeleteAPIView(DestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostDetailSerializers
    lookup_field = 'slug'


class PostCreateAPIView(CreateAPIView):

    serializer_class = PostCreateUpdateSerializers
    #permission_classes = [IsAuthenticated, IsAdminUser]
    def perform_create(self, serializer):
        import pdb;pdb.set_trace()
        serializer.save()
#    def create(self, request, *args, **kwargs):
        #import pdb;pdb.set_trace()
    #    serializer = self.get_serializer(data=request.data)
    #    serializer.is_valid(raise_exception=True)
    #    self.perform_create(serializer)
    #    headers = self.get_success_headers(serializer.data)
    #    return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)




class PostListAPIView(ListAPIView):
    serializer_class = PostListSerializers
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title','content', 'user__first_name']
    #pagination_class = PostPageNumberPagination #LimitOffsetPagination# PageNumberPagination


    def get_queryset(self, *args, **kwargs):
        queryset_list = Post.objects.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(title__icontains=query) |
                Q(content__icontains=query) |
                Q(user__first_name__icontains=query) |
                Q(user__last_name__icontains=query)
            ).distinct()
        return queryset_list
