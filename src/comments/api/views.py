from posts.api.permissions import IsOwnerOrReadOnly
from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
)
from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
 )
from posts.api.pagination import (
    PostLimitOffsetPagination,
    PostPageNumberPagination,
)
from .serializer import (
    CommentSerializer,
    CommentListSerializer,
    CommentDetailSerializer,
    create_comment_serializer,
  #  CommentEditSerializer,
)

from comments.models import Comment

from django.db.models import Q
from rest_framework.filters import (
    SearchFilter,
    OrderingFilter,
)
from rest_framework.mixins import (
    DestroyModelMixin,
    UpdateModelMixin,
)
from posts.api.permissions import IsOwnerOrReadOnly
class CommentCreateAPIView(CreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentDetailSerializer
    #permission_classes = IsAuthenticated

    def get_serializer_class(self):
        model_type = self.request.GET.get("type")
        slug = self.request.GET.get("slug")
        parent_id = self.request.GET.get("parent_id", None)
        return create_comment_serializer(
                model_type=model_type,
                slug=slug,
                parent_id=parent_id,
                user=self.request.user
)

class CommentDetailAPIView(   RetrieveAPIView,
        UpdateModelMixin,
        DestroyModelMixin):

        queryset = Comment.objects.filter(id__gte=0)
        serializer_class = CommentDetailSerializer
        permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
        lookup_field = 'pk'

        def put(self, request , *args,**kwargs):
            return self.update(request, *args, **kwargs)

        def destroy(self, request, *args, **kwargs):
            return self.destroy(request, *args, **kwargs)


class CommentListAPIView(ListAPIView):
    serializer_class = CommentListSerializer
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['content', 'user__first_name']
    pagination_class = PostPageNumberPagination #LimitOffsetPagination# PageNumberPagination


    def get_queryset(self, *args, **kwargs):
        queryset_list = Comment.objects.filter(id__gte=0) #.all()
        query = self.request.GET.get("q")
        if query:
            queryset_list = queryset_list.filter(
                Q(content__icontains=query) |
                Q(user__first_name__icontains=query) |
                Q(user__last_name__icontains=query)
            ).distinct()
        return queryset_list
