module.exports = {
    login: function (username, password, cb) {
        if(localStorage.token){
            if(cb)cb(True)
            return
        }
        this.getToken(username,password, (res)=>{
            if( res.authenticated){
                localStorage.token = res.token;
                if (cb)cb(true)
            }else {
                if(cb)cb(false)
            }

        })
    },
    logout: function () {
        delete localStorage.token
    },
    loggedIn: function () {
        return !!localStorage.token
    },
    getToken: function (username, password, cb) {
        $.ajax({
            type: 'POST',
            url: 'api/auth/token/',
            data: {
                username: username,
                password: password
            },
            success: function (res) {
                cb({
                    authenticated: true,
                    token: res.token
                })
            }
        })
    }
};
