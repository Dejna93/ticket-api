var React = require('react');
var auth = require('./auth');

module.exports = React.createClass({
    getInitialState: function () {
        return {'user' : []}
    },
    componentDidMount: function () {
        this.loadUserData()
    },
    contextTypes: {
        router: React.PropTypes.object.isRequired
    },
    logoutHandler: function () {
        auth.logout()
        this.context.router.replace('/login')
    },
    loadUserData: function () {
        $.ajax({
            method: 'GET',
            url: '/api/user/login',
            dataTypes: 'json',
            headers: {
                'Authorization':'Token' + localStorage.token
            },
            success: function (res) {
                this.setState({user:res})
            }.bind(this)
        })
    },
    render: function () {
        return (
            <div>
                <h3>You are now logged in</h3>
                <button onclick={this.logoutHandler}>Log out</button>
            </div>
        )
    }
});