'use strict';


// tutorial9.js
var TicketBox = React.createClass({
    loadCommentsFromServer: function () {
        $.ajax({
            url     : this.props.url,
            dataType: 'json',
            cache   : false,
            success : function (data) {
                this.setState({data: data});
            }.bind(this),
            error   : function (xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState       : function () {
        return {data: []};
    },
    componentDidMount     : function () {
        this.loadCommentsFromServer();
        setInterval(this.loadCommentsFromServer, this.props.pollInterval);
    },
    render                : function () {
        return (
            <div className="commentBox">
                <h1>Zgłoszenia</h1>
                <TicketList data={this.state.data}/>
                <TicketForm />
            </div>
        );
    }
});

var TypeList = React.createClass({
    render: function () {
        var types = [
            {id: 1, name: "Serwisowe"},
            {id: 2, name: "Projekt wewnętrzny"},
            {id: 3, name: "Projekt zewnętrzny"},
        ];
        var typesNodes = types.map(function (type) {
            return (
                <Type id={type.id} name={type.name}>
                </Type>
            );
        });
        return (

            <select className="form-control col-xd-4" name="type">
                {typesNodes}
            </select>
        );
    }
});
var Type = React.createClass({
    render: function () {
        return (
            <option value={this.props.id}>{this.props.name} </option>
        );
    }
});

var StateList = React.createClass({
    render: function () {
        var states = [
            {id: 1, name: "Nowy"},
            {id: 2, name: "W przygotowaniu"},
            {id: 3, name: "Opóźnione"},
            {id: 4, name: "Testy"},
            {id: 5, name: "Zamknięty"},
        ];
        var stateNodes = states.map(function (state) {
            return (
                <State id={state.id} name={state.name}/>
            )
        });
        return (
            <select className="form-control col-xd-4" name="state">
                {stateNodes}
            </select>

        );
    }
});

var State = React.createClass({
    render: function () {
        return (
            <option value={this.props.id}>{this.props.name} </option>
        );
    }
});

var TicketForm = React.createClass({

    render: function () {
        return (
            <form className="form-horizontal col-xs-6 form-ticket">
                <div className="form-group">
                    <h3 className="text-center">Zgłoszenie</h3>
                </div>
                <div className="form-group">
                    <label for="type" className="control-label col-xs-2">Tytuł</label>
                    <div className="col-xs-8">
                        <input type="text" className="form-control" id="type" placeholder="Tytuł"/>
                    </div>
                </div>
                <div className="form-group ">
                    <label className="control-label col-xs-2" for="type">Typ</label>
                    <div className="col-xs-5">
                        <TypeList/>
                    </div>
                </div>

                <div className="form-group ">
                    <label className="control-label col-xs-2" for="type">State</label>
                    <div className="col-xs-5">
                        <StateList/>
                    </div>
                </div>
                <div className="form-group">
                    <label className="control-label col-xs-2" for="type">Zgłoszenie</label>
                    <div className="col-xs-10">
                        <textarea className="form-control col-xs-2" rows="4" cols="50"/>
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-xs-offset-4 col-xs-8">

                        <button type="submit" className="btn btn-primary">Wyślij</button>

                    </div>

                </div>
            </form>
        );
    }
});
var TicketList = React.createClass({
    render: function () {
        console.info(this.props.data)
        var commentNodes = this.props.data.map(function (ticket) {
            return (
                <Ticket title={ticket.title} slug={ticket.slug} id={ticket.id} content={ticket.content}
                        state={ticket.state} type={ticket.type} publish={ticket.publish} user={ticket.user.username}
                >
                    {ticket.type}
                </Ticket>
            );
        });
        return (
            <div className="commentList">
                {commentNodes}
            </div>
        );
    }
});

var Ticket = React.createClass({

    render: function () {
        return (

            <div className="col-lg-8 ticket" >
                <h2 className="ticket-title" >
                    <a className="ticket-title" href={this.props.slug}>{this.props.title} </a>
                </h2>
                <p className="ticket-title-meta">{this.props.publish} | {this.props.type} | {this.props.state}</p>
                <p>Zgłaszający: {this.props.user} </p>
                <div>
                    <p className="ticket-content">{this.props.content}</p>
                </div>
                <div className="btn-group">
                    <a className="btn btn-primary">Odpowiedz</a>
                    <a className="btn btn-info">Zmien status</a>
                    <a className="btn btn-danger">Zamknij</a>
                </div>
            </div>
        );
    }
});


ReactDOM.render(
    <TicketBox url="/api/posts"/>,
    document.getElementById('content')
);
