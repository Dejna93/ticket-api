'use strict';


// tutorial9.js
var TicketBox = React.createClass({
  loadCommentsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadCommentsFromServer();
    setInterval(this.loadCommentsFromServer, this.props.pollInterval);
  },
  render: function() {
    return (
      <div className="commentBox">
        <h1>Comments</h1>
        <TicketList data={this.state.data} />
        <TicketForm />
      </div>
    );
  }
});


var TicketForm = React.createClass({
  render: function() {
    return (
       <form className="commentForm">
        <input type="text" placeholder="Your name" />
        <input type="text" placeholder="Say something..." />
        <input type="submit" value="Post" />
      </form>
    );
  }
});
var TicketList = React.createClass({
  render: function() {
      console.info(this.props.data)
    var commentNodes = this.props.data.map(function(ticket) {
      return (
        <Ticket title={ticket.title} slug={ticket.slug} id={ticket.id} content= {ticket.content}  state={ticket.state} type={ticket.type}>
            {ticket.type}
        </Ticket>
      );
    });
    return (
      <div className="commentList">
        {commentNodes}
      </div>
    );
  }
});

var Ticket = React.createClass({

  render: function() {
    return (

      <div class="col-lg-6 ticket" className="ticket">
        <h2 class="ticket-title" className="ticketTitle">
         <a class="ticket-title" href={this.props.slug} >{this.props.title} </a>
        </h2>
          <p class="ticket-title-meta">{this.props.type}  {this.props.state}</p>
          <div class="ticket-content">
              {this.props.content}
          </div>

      </div>
    );
  }
});




ReactDOM.render(
  <TicketBox url="/api/posts" />,
  document.getElementById('content')
);
