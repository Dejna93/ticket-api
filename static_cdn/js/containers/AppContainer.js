import React from 'react'
import Navbar from '../components/Navbar'
import authentication from '../services/authentication'
import eventManager from '../services/event_manager'

const styles ={

}

const AppContainer = React.createClass({
    contextTypes: {
        router : React.PropTypes.object.isRequired
    },
    getInitialState(){
        return {
            loggedIn: false
        }
    },
    updateAuth(loggedIn){
        this.setState({
            loggedIn:loggedIn
        })
    },
    componentDidMount(){
        this.subscription = eventManager.getEmitter().addListener(
            eventManager.authChannel,
            this.updateAuth
        );
        const promise = authentication.isAuthenticated();
        promise.then(resp => {this.setState({loggedIn:true})})
            .catch(err => {this.setState({loggedIn: false})});

    },

    componentWillMount(){
        this.subscription.remove();
    },

    render(){
        return (
            <div clasName="container">
            <Navbar loggedIn={this.state.loggedIn}/>
                <div>
                    {React.cloneElement(this.props.children , {loggedIn: this.state.loggedIn})}
                </div>

            </div>
        )
    }
});

export default AppContainer
