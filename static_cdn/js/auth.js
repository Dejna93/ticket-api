var LoginForm = React.createClass({
    loginToServer : function () {
        $.ajax({
            type        :   'POST',
            url         :   '/api/users/login',
            dataType    :   'json',
            cache       :   false,
            success     :   function (user) {
                console.info(data)
                var _token = data.token;
                var _decoded = jwt_decode(_token);

                this.setState({user : user});
            }.bind(this),
            error       :   function (xhr, status, err) {
                console.error(this.url, status , err.toString());
            }.bind(this)
        });
    },

    handleSubmit: function(e){
        e.preventDefault();
         console.info('send');
        var username = this.refs.username.value.trim();
        var password = this.refs.password.value.trim();
        console.info('presend');
        if(!username || !password){
            console.error('Username or password not exists')
            return;
        }
        //TODO: SEND REQUEST TO THE SERVER

         $.ajax({
            type        :   'POST',
            url         :   '/api/users/login',
            dataType    :   'json',
            cache       :   false,
            success     :   function (user) {
                console.info(data)
                var _token = data.token;
                var _decoded = jwt_decode(_token);

                this.setState({user : user});
            }.bind(this),
            error       :   function (xhr, status, err) {
                console.error(this.url, status , err.toString());
            }.bind(this)
        });
        this.refs.username.value ='';
        this.refs.password.value ='';
        return;

    },

   render(){
       return (
          <form className="form-horizontal col-xs-6 form-ticket" onsubmit={this.handleSubmit}>
                <div className="form-group">
                    <h3 className="text-center">Login</h3>
                </div>
                <div className="form-group">
                    <label for="type" className="control-label col-xs-2">Login</label>
                    <div className="col-xs-8">
                        <input type="text" className="form-control" ref='username' placeholder="Username/Email"/>
                    </div>
                </div>
                <div className="form-group">
                    <label for="type" className="control-label col-xs-2">Password</label>
                    <div className="col-xs-8">
                        <input type="password" className="form-control" ref="password" id="type" placeholder="Password"/>
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-xs-offset-4 col-xs-8">

                        <button type="submit" className="btn btn-primary">Login</button>

                    </div>

                </div>
            </form>
       )
   }
});

ReactDOM.render(
    <LoginForm/>,
    document.getElementById('login')
);
